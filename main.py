import webapp2
from google.appengine.api import users,memcache
from google.appengine.api.channel import channel
import controller
from gmail import Auth
import model
import httplib2
from gaesessions import get_current_session,set_current_session
import json
import datetime
 

class LoginPage(controller.Controller):
    def get(self):
        session = get_current_session()

        if session.is_active():
            self.redirect('/startpage')
        else:
            self.template_renderer('signin.html')


class Gmail(controller.Controller):
    def get(self):
        url = Auth.get_url()
        self.redirect(url)


class Redirect(controller.Controller):
    def get(self):
        session = get_current_session()
        credential = Auth.credentials(self.request.get("code"))
        email = credential.id_token['email']
        user_name = credential.id_token['name']
        session['user_email'] = str(email)
        session['user_name'] = str(user_name)

        if not model.Users.existing_user(email):
            model.Users.store_credential(email, user_name)
        
        self.redirect('/startpage')


class StartPage(controller.Controller):
    def get(self):
        session = get_current_session()
        token_obj = RequestToken()
        # import logging
        # logging.info("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[")
        token = token_obj.get_token()
        logging.info(token_obj.client_id)
        client_id = token_obj.client_id
        email = session['user_email']
        if model.Channels.check_client_email(email):
            model.Channels.update_client_id(email, client_id)
        else:
            model.Channels.store_client_id(email, client_id)

        if model.Users.check_user(session.get('user_email')):
            friend  = model.Users.get_users(session.get('user_email'))

            user = {
                    "friends":friend,
                    "token": token,
                    "client_id": client_id,
                    "sender_email": session['user_email']
                    }    

            self.template_renderer('startpage.html',user )
        else:
            self.redirect('/logout')


class Person(controller.Controller):
    # def get(self,user_id):
    #     receiver = model.Users.get_user_email(user_id)
    #     session = get_current_session()
    #     message, message_id = model.Message.retrive_message(session['user_email'], receiver)
    #     session['message_id'] = str(message_id)
    #     user_email = session['user_email']
        
    #     data = {
    #             'messages': message,
    #             'message_id': session['message_id'],
    #             'receiver_email': receiver,
    #             'sender_email': session['user_email'],
    #             }

    #     self.template_renderer('chatpage.html',data)

    def post(self):
        receiver = self.request.get("receiver")
        session = get_current_session()
        message, message_id = model.Message.retrive_message(session['user_email'], receiver)
        session['message_id'] = str(message_id)
        user_email = session['user_email']
        # messages =[]
        # for i in message:
        #     messages.append(i.__dict__)
        # print messages
        data = {
                'messages': message,
                'message_id': session['message_id'],
                'receiver_email': receiver,
                'sender_email': session['user_email']
               
                }
        self.template_renderer('chatpage.html',data)        
        # self.response.content_type = 'application/json'
        # self.response.write(json.dumps(data))


class StoreMessage(controller.Controller):
    def post(self):
        session = get_current_session()
        receiver = str(self.request.get("receiver"))
        message = str(self.request.get("message"))
        # print type(receiver),receiver
        user_email = str(session['user_email'])
        model.Message.store_message(user_email,receiver, 
                                    message)
        client_id = model.Channels.get_client_id(receiver)
        # print client_id
        data = {
                'sender':user_email,
                'message':message
                }

        # print data
        # print client_id,"..................."
        channel.send_message(client_id, json.dumps(data))


class Logout(controller.Controller):
    def get(self):
        session = get_current_session()

        if session.is_active():
            session.terminate()
        self.redirect('/')


# google channel api application from here

class RequestToken(controller.Controller):
    def get_token(self):
        session = get_current_session()
        user_id = model.Users.get_user_id(session['user_email'])
        self.client_id = str(datetime.datetime.now()) + str(user_id)
        token = channel.create_channel(self.client_id,duration_minutes=100)
        return token


class CreateToken(controller.Controller):
    def post(self):
        # client_id = self.request.get("client_id")
        # email = self.request.get("email")
        token_obj = RequestToken()
        token = token_obj.get_token()
        client_id = token_obj.client_id
        email = session['user_email']
        
        if model.Channels.check_client_email(email):
            model.Channels.update_client_id(email, client_id)
        else:
            model.Channels.store_client_id(email, client_id)

        data = {
                "token": token,
                "client_id": client_id
                }

        self.response.content_type = 'application/json'
        self.response.write(json.dumps(data))
#         # print client_id
  

class Temp(controller.Controller):
    def post(self):
        client_id = self.request.get("client_id")
        email = self.request.get("sender_email")
        if model.Channels.check_client_email(email):
            model.Channels.update_client_id(email, client_id)
        else:
            model.Channels.store_client_id(email, client_id)

# class RequestMessage(controller.Controller):
#     def post(self):
#         client_id = self.request.get('client_id')
#         message_id = self.request.get('message_id')
#         session = get_current_session()
#         receiver = self.request.get("receiver_email")
#         # print receiver +"hello"
#         temp = datetime.datetime.strptime(str(session['message_id']),
#                                             '%Y-%m-%d %H:%M:%S.%f')
#         messages, message_id = model.Message.polled_messages( session['user_email'],
#                                                                 receiver, temp)
#         # print message_id
        
#         session['message_id'] = str(message_id)
#         list_msg = []

#         for i in messages:
#             list_msg.append(i.message)
        
#         no_of_msg = len(list_msg)
#         messages = list_msg
#         obj = {
#                 'message': messages, 
#                 'length': no_of_msg,
#                 'message_id':session['message_id']
#                 }  

        # print "hello"
        # print msg_id, client_id
        # print client_id, msg_id
        # obj = {'msg':'hello'}
        # channel.send_message(client_id, json.dumps(obj) )


# ajax polling method uncomment this............................


# class PolledMessages(controller.Controller):
#     def post(self):
#         session = get_current_session()
#         receiver = self.request.get("receiver")
#         # print receiver +"hello"
#         temp = datetime.datetime.strptime(str(session['message_id']),
#                                             '%Y-%m-%d %H:%M:%S.%f')
#         messages, message_id = model.Message.polled_messages( session['user_email'],
#                                                                 receiver, temp)
#         session['message_id'] = str(message_id)
#         list_msg = []

#         for i in messages:
#             list_msg.append(i.message)
        
#         no_of_msg = len(list_msg)
#         messages = list_msg
#         self.response.content_type = 'application/json'
#         obj = {
#             'message': messages, 
#             'length': no_of_msg,
#             'message_id':session['message_id']
#           } 
#         self.response.write(json.dumps(obj))

# Till here..........................
