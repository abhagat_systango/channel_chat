from google.appengine.ext import db
from oauth2client.contrib.appengine import CredentialsProperty   
from google.appengine.api import memcache
import pdb
import datetime

class Channels(db.Model):
    email = db.StringProperty()
    client_id = db.StringProperty()

    @staticmethod
    def store_client_id(email, client_id):
        channel_obj = Channels()
        channel_obj.email = email
        channel_obj.client_id = client_id
        channel_obj.put()

    @staticmethod
    def get_client_id(email):
        query_obj = Channels().all().filter('email =',email)
        # print email
        query_list = query_obj.fetch(1)
        # print query_list
        return query_list[0].client_id


    @staticmethod
    def update_client_id(email, client_id):
        query_obj = Channels().all().filter('email =',email).fetch(1)
        query_key = query_obj[0].key()
        temp = query_obj[0].get(query_key)
        # print query_key
        temp.client_id = client_id
        temp.put()

    @staticmethod
    def check_client_email(email):
        query_obj = Channels().all().filter('email =',email)
        if query_obj.get() == None:
            return False
        return True
        
# class temp(db.Model):
#     l = db.ListProperty(str,indexed=False)

#     @staticmethod
#     def l():
#         obj = temp()
#         obj.l = ['1','2']
#         obj.put()

#     @staticmethod
#     def t():
#         obj = temp().all().get()
#         print obj.__dict__




class Users(db.Model):
    email = db.StringProperty()
    user_name = db.StringProperty()

    @staticmethod
    def store_credential(email, user_name):
        user = Users()
        user.email = email
        user.user_name = user_name
        user.put()

    @staticmethod
    def existing_user(email):
        user = Users().all().filter("email = ", email).get()
        return user


    @staticmethod
    def get_users(email):
        users = Users().all().filter('email !=', email)
        return users

    @staticmethod
    def check_user(email):
        user = Users().all().filter('email =', email).get()
        if user is None:
            return False
        return True 

    @staticmethod
    def get_user_id(email):
        user = Users().all().filter('email =', email).fetch(1)
        print email
        return user[0].key().id()

    @staticmethod
    def get_user_email(user_id):
        # print user_id
        user = Users().get_by_id(int(user_id)) 
        # print user.email
        return user.email

class Message(db.Model):
    sender = db.StringProperty()
    message = db.StringProperty()
    receiver = db.StringProperty()
    order = db.DateTimeProperty(auto_now_add=True)

    @staticmethod
    def store_message(sender_email,receiver_email, message):
        message_obj = Message()
        message_obj.sender = sender_email
        message_obj.receiver = receiver_email
        message_obj.message = message
        message_obj.put()

    @staticmethod
    def polled_messages(sender_email, receiver_email,message_id):
        messages_query = Message().all().filter("sender = ", receiver_email).filter( "receiver = ", sender_email).filter("order > ",message_id).order('order')
        rev_message = message_id

        messages_obj = messages_query.fetch(1)
        if messages_obj != []:
            rev_message =messages_obj[0].order
        # print rev_message
        # pdb.set_trace()
        return messages_query ,rev_message



    @staticmethod
    def retrive_message(sender_email, receiver_email):
        messages_query = Message().all().filter("sender IN ", (sender_email, receiver_email) ).filter( "receiver IN ",(sender_email, receiver_email)).order("order")
        rev_message = datetime.datetime.strptime(str('2016-07-01 01:50:40.002145'),
                                                    '%Y-%m-%d %H:%M:%S.%f')
        
        # print messages_query.fetch(199)
        # pdb.set_trace()
        messages_obj = []
        if messages_query.count():
            messages_obj = messages_query.fetch(1,messages_query.count()-1)
        if messages_obj != []:
            rev_message =messages_obj[0].order
        # print rev_message 
        # print "                 hererer  er  re re r"
        
        return messages_query.fetch(1000) ,rev_message
